<?php

namespace Drupal\action_link\Plugin\ActionLinkOutput;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Action Link Output plugins.
 */
abstract class ActionLinkOutputBase extends PluginBase implements ActionLinkOutputInterface {

}
