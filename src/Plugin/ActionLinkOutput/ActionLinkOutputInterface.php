<?php

namespace Drupal\action_link\Plugin\ActionLinkOutput;

use Drupal\action_link\Entity\ActionLinkInterface;
use Drupal\Component\Plugin\DerivativeInspectionInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Interface for Action Link Output plugins.
 *
 * An Action Link Output plugin provides a place to display an action link.
 *
 * An action link can be configured to use one or more output plugins.
 */
interface ActionLinkOutputInterface extends PluginInspectionInterface, DerivativeInspectionInterface {

  /**
   * Determines whether the plugin is usable with the action link entity.
   *
   * @param \Drupal\action_link\Entity\ActionLinkInterface $action_link
   *   An action link entity.
   *
   * @return bool
   *   TRUE if the output plugin can be used with the given action link entity,
   *   FALSE otherwise.
   */
  public static function appliesToActionLink(ActionLinkInterface $action_link): bool;

}
